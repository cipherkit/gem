class String
  def is_alpha?
    !!match(/^[[:alnum:]]+$/)
  end

  def is_num?
    !!match(/^[[:digit:]]+$/)
  end
end
