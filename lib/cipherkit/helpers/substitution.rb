module CipherKit
  module Helpers
    class Substitution
      def move_all_by_n(string:, index:)
        begin
          raise "index parameter has to be either a positive or a negative int" unless index.to_s.is_num?
          arr = string.split(//)
          return Hash[arr.zip(arr.rotate(index.to_i))]
        rescue Exception => e
          printf "Caught: #{e}"
        end # rescue Exception=>e
      end

      def place_keyword_at(keyword:, string:, index:)
        begin
          if index.to_s.is_num?
            return string.insert(index.to_i, keyword)
          elsif index.is_alpha?
            if index == "start"
              return string.insert(0, keyword)
            elsif index == "end"
              return string.insert(-1, keyword)
            else
              raise "index parameter has to be either a positive int, a negative int, 'start' or 'end'"
            end # index == "start"
          end # index.is_num?
        rescue Exception => e
          printf "Caught: #{e}"
        end # rescue Exception=>e
      end # place_keyword_at
    end # Substitution
  end # Helpers
end # CipherKit
