module CipherKit
  module Ciphers
    class ROT
      def self.encode string:, key:, alphabet:
         encrypter = CipherKit::Helpers::Substitution.move_all_by_n(string: alphabet, index: key)
         string.chars.map { |c| encrypter.fetch(c, c) }
      end

      def self.decode string:, key:, alphabet:
        self.encode string: string, key: -(key.abs), alphabet: alphabet
      end
    end # ROT
  end # Helpers
end # CipherKit
