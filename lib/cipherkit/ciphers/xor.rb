module CipherKit
  module Ciphers
    class XOR
      def self.encode string:, key:
        string.split(//).collect {|e| [e.unpack('C').first ^ (key.to_i & 0xFF)].pack('C') }.join
      end

      def self.decode string:, key:
        self.encode string: string, key: key
      end
    end # XOR
  end # Helpers
end # CipherKit
